= gcip Documentation
:doctype: book

The Gitlab CI Python Library Documentation

* link:./user/index.html[User Documentation]
* link:./api/gcip/index.html[API Reference]
