from gcip import (
    Image,
    PagesJob,
    Pipeline,
    PredefinedVariables,
)
from gcip.addons.gitlab.jobs import pages as gitlab_pages
from gcip.addons.python.jobs.test import (
    Pytest,
    EvaluateGitTagPepe440Conformity,
)
from gcip.addons.python.jobs.build import BdistWheel
from gcip.addons.python.jobs.deploy import TwineUpload
from gcip.addons.python.jobs.linter import (
    Mypy,
    Isort,
    Flake8,
)
from gcip.addons.container.sequences import build

pipeline = Pipeline()
pipeline.initialize_image("python:3.7-slim")

pipeline.add_children(
    Isort(),
    Flake8(),
    Pytest(),
    Mypy("gcip"),
    # pydoc3 tries to resolve all Python objects and their attributes.
    # This includes PredefinedVariables class which are not necessarily set in every pipeline,
    # like CI_DEBUG_TRACE or CI_DEPLOY_PASSWORD . Resolving those variables lead to a KeyError.
    # When CI is unset, then PredefinedVariables behave different, returns mock values for all
    # other unset variables. That makes pdoc3 running without stumbling over those Key errors.
    gitlab_pages.Pdoc3(module="gcip", output_path="/api").prepend_scripts("unset CI"),
    gitlab_pages.AsciiDoctor(source="docs/index.adoc", out_file="/index.html", name="pages_index"),
    gitlab_pages.AsciiDoctor(source="docs/user/index.adoc", out_file="/user/index.html", name="pages_user_index"),
    BdistWheel(),
)

# gitlabci-local only works with 'sh' as kaniko and crane entrypoint
kaniko_image = None
crane_image = None
if PredefinedVariables.CI_COMMIT_REF_SLUG == "gitlab-local-sh":
    kaniko_image = Image("gcr.io/kaniko-project/executor:debug", entrypoint=["sh"])
    crane_image = Image("gcr.io/go-containerregistry/crane:debug", entrypoint=["sh"])

pipeline.add_children(
    build.FullContainerSequence(
        image_name="thomass/gcip",
        kaniko_kwargs={"kaniko_image": kaniko_image},
        crane_kwargs={"crane_image": crane_image},
        do_crane_push=PredefinedVariables.CI_COMMIT_TAG is not None or PredefinedVariables.CI_COMMIT_REF_NAME == "main",
    ),
    name="gcip",
)

if PredefinedVariables.CI_COMMIT_TAG:
    pipeline.add_children(
        EvaluateGitTagPepe440Conformity(),
        TwineUpload(),
    )

pipeline.add_children(PagesJob())
pipeline.write_yaml()
