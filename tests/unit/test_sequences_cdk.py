from gcip import Pipeline
from tests import conftest
from gcip.addons.aws.sequences.cdk import DiffDeploy


def test_diff_deploy_multiple_stacks() -> None:
    pipeline = Pipeline()
    pipeline.add_children(DiffDeploy("stack1", "stack2", toolkit_stack_name="toolkit-stack"))
    conftest.check(pipeline.render())


def test_diff_deploy_with_context() -> None:
    pipeline = Pipeline()
    pipeline.add_children(DiffDeploy("teststack", toolkit_stack_name="toolkit-stack", context={"foo": "bar", "abra": "kadabra"}))
    conftest.check(pipeline.render())


def test_diff_deploy_with_options() -> None:
    pipeline = Pipeline()
    pipeline.add_children(DiffDeploy("teststack", toolkit_stack_name="toolkit-stack", diff_options="-d", deploy_options="-e"))
    conftest.check(pipeline.render())
