from tests import conftest
from gcip.addons.gitlab.jobs import pages as gitlab_pages


def test_asciidoctor(pipeline):
    pipeline.add_children(
        gitlab_pages.AsciiDoctor(source="docs/index.adoc", out_file="/index.html"),
        gitlab_pages.AsciiDoctor(source="docs/awesome.adoc", out_file="/awesome.html", name="pages_awesome"),
    )
    conftest.check(pipeline.render())


def test_pages_pdoc3(pipeline):
    pipeline.add_children(
        gitlab_pages.Pdoc3(module="gcip"),
        gitlab_pages.Pdoc3(module="userdoc", output_path="/user", name="userdoc"),
    )
    conftest.check(pipeline.render())


def test_pages_sphinx(gitlab_ci_environment_variables, pipeline):
    pipeline.add_children(gitlab_pages.Sphinx())
    conftest.check(pipeline.render())
