import gcip
from tests import conftest
from gcip.addons.aws.sequences.cdk import DiffDeploy


def test():
    pipeline = gcip.Pipeline()
    pipeline.add_children(DiffDeploy("my-cdk-stack", toolkit_stack_name="cdk-toolkit"))

    conftest.check(pipeline.render())
